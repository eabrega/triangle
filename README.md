# Тестовое задание #

## ТЗ ##

При вычислении площади треугольника небходимо соблюдать следующие условия

1. Длинна ребра не может быть равно 0
1. Длинна ребер должна позволять создать замкнутую фигуру
1. Треугольник должен быть прямым

Первые два правила справедливы для всех фигур и были вынесены в базовый класс. От которого унаследован класс "Прямоугольный Треугольник". Объект которого может быть создан только при условии выполнения всех трех правил валидации. 

Расчет площади решено осуществлять формулой общего вида, подходящей для любого треугольника.
Это сократит количесво потенциально уязвимих ошибок в коде.

## SQL ##

```sql 
create table Goods (id int, id_Bayer int, Name text, DataPayment DATE);
insert into Goods values 
  (0, 1, 'Молоко', '2017-12-02'),  -- Sasha
  (1, 0, 'Сметана', '2017-12-7'), -- brega
  (2, 0, 'Молоко', '2017-12-03'), -- brega
  (3, 1, 'Сметана', '2017-12-04'),-- Sasha
  (4, 1, 'Сметана', '2017-02-05'),-- Sasha 
  (5, 0, 'Молоко', '2017-03-06'), -- brega
  (6, 2, 'Молоко', '2017-02-07'); -- Oleg
create table Bayer (id int, Name text);
insert into Bayer values (0, 'Brega'), (1, 'Sasha'), (2, 'Oleg');

select Bayer.Name From Bayer
join Goods on Bayer.id = Goods.id_Bayer
  where Goods.Name Like '%Молоко%' and Bayer.Name not in (
    select Bayer.Name From Bayer
    inner join Goods on Bayer.id = Goods.id_Bayer
    where Goods.Name Like '%Сметана%' and 
      DataPayment > LAST_DAY(CURDATE()) + INTERVAL 0 DAY - INTERVAL 1 MONTH and
      DataPayment < DATE_ADD(LAST_DAY(CURDATE()), INTERVAL 1 DAY)  
    group by Bayer.Name
  )
group by Bayer.Name;
```
