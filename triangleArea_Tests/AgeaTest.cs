﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using triangleArea;

namespace triangleArea_Tests
{
    [TestClass]
    public class AgeaTest
    {
        [TestMethod]
        public void NewTriangle_ZeroInsertParam()
        {
            Assert.ThrowsException<Exception>(() => { Triangle test = new Triangle(0, 0, 0); });
            Assert.ThrowsException<Exception>(() => { Triangle test = new Triangle(3, 4, 0); });
            Assert.ThrowsException<Exception>(() => { Triangle test = new Triangle(4, 3, 0); });
            Assert.ThrowsException<Exception>(() => { Triangle test = new Triangle(3, 0, 4); });

        }
        [TestMethod]
        public void NewTriangle_ImposibleTriangle()
        {

            Assert.ThrowsException<Exception>(() => { Triangle test = new Triangle(1, 1, 100); });


        }
        [TestMethod]
        public void NewRectTriangle_NotRect()
        {
            // создадим острый, равнобедренный треугольник
            Assert.ThrowsException<Exception>(() => { RectTriangle test = new RectTriangle(3, 4, 4); });


        }
        [TestMethod]
        public void Triangle_GetArea()
        {

            Triangle test = new Triangle(5.1234, 5.4321, 5.8888);

            var Area = test.GetArea();

            // 

            Assert.IsTrue(Equals(test.GetArea(), 12.88), $"S - {Area}");

        }
        /// <summary>
        /// Классический Египетский треугольник, S = 6
        /// </summary>
        [TestMethod]
        public void RectTriangle_GetArea()
        {

            Triangle test = new Triangle(3, 4, 5);

            var Area = test.GetArea();

            Assert.IsTrue(Equals(Area, 6.0), $"Area - {Area}");

        }
    }
}
