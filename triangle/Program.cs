﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace triangleArea
{
    public class Triangle
    {

        public (double a, double b, double c) Edges { get; }
        /// <summary>
        /// Полупериметр
        /// </summary>
        double p {
            get {
                return (Edges.a + Edges.b + Edges.c) / 2;
            }
        }

        public Triangle(double a, double b, double c)
        {
            if (a > 0 && b > 0 && c > 0) Edges = (a, b, c);
            else throw new Exception("Длинна ребра должна быть больше 0!");


            if (!isRealTreangle()) throw new Exception("Треугольник с такими сторонами не возможен!");
        }


        public bool isRealTreangle()
        {
            if ((Edges.a + Edges.b <= Edges.c) || (Edges.a + Edges.c <= Edges.b) || (Edges.b + Edges.c <= Edges.a)) return false;
            return true;
        }
        /// <summary>
        /// Нахождение площади любого треугольника, по формуле Герона.
        /// </summary>
        /// <returns></returns>
        virtual public double GetArea()
        {
            double S = 0;
            if (isRealTreangle())
            {
                S = Math.Sqrt(p * (p - Edges.a) * (p - Edges.b) * (p - Edges.c));
            }

            return Math.Round(S, 2);
        }

    }

    public class RectTriangle : Triangle
    {

        public RectTriangle(double a, double b, double c) : base(a, b, c)
        {   // создадим треугольник
            var katets = new List<double>() { a, b, c, };
            var H = katets.Max();

            katets.RemoveAt(katets.IndexOf(katets.Max()));

            for (byte i = 0; i != 2; i++)
            {
                katets[i] = katets[i] * katets[i];
            }
            var sqare_hypotenuse = Math.Round(H * H, 2);

            // сумма квадратов катетов должна равняться квадрату гипотинузы (Н)
            if (Equals(Math.Round(katets.Sum(), 2), sqare_hypotenuse) == false) throw new Exception("Данный треугольник не прямоугольный!");
        }
        /// <summary>
        /// Если вычисление общим методом окажется слишком накладным, можно реализовать методы на каждый частный случай.
        /// Однако надежность может снизиться - больше мест для ошибок. 
        /// </summary>
        /// <returns></returns>
        public override double GetArea()
        {
            return base.GetArea();
        }
    }

    class Program
    {
        static void Main()
        {
            try
            {
                RectTriangle myRectTriangle = new RectTriangle(3.1111, 4.1111, 5.1555);


                Console.WriteLine($"Площадь прямоугольного треугольника - {myRectTriangle.GetArea():0.00}");
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
            }
            finally
            {

                Console.ReadLine();

            }

        }
    }
}
